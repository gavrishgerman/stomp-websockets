import org.junit.Test;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.converter.StringMessageConverter;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.lang.reflect.Type;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class Tests {

    @Test
    public void stomp() throws ExecutionException, InterruptedException {
        WebSocketClient transport = new StandardWebSocketClient();
        WebSocketStompClient stompClient = new WebSocketStompClient(transport);
        stompClient.setMessageConverter(new StringMessageConverter());
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
//        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
//        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        TaskScheduler taskScheduler = new ConcurrentTaskScheduler(new ScheduledThreadPoolExecutor(10));
        stompClient.setTaskScheduler(taskScheduler); // for heartbeats, receipts

        String url = "ws://localhost:8080/portfolio";
        StompSessionHandler handler = new StompSessionHandlerAdapter() {
            @Override
            public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
                super.afterConnected(session, connectedHeaders);
                System.out.println("connected");
//                session.send("/app/greeting", "hi");
            }
            
        };

        ListenableFuture<StompSession> connect = stompClient.connect(url, handler);
        StompSession stompSession = connect.get();
        stompSession.setAutoReceipt(true);
        stompClient.start();
        System.out.println(stompSession.getSessionId());
        stompSession.subscribe("/topic/greeting", new StompFrameHandler() {

            @Override
            public Type getPayloadType(StompHeaders headers) {
                return String.class;
            }

            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                // ...
                System.out.println("handling");
                System.out.println(payload);
            }


        });

        StompSession.Receiptable hi = stompSession.send("/app/greeting/", new TextMessage("hi"));
        hi.addReceiptTask(() -> System.out.println("recieved"));

    }
}
