package hello;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
public class GreetingController {


//    @MessageMapping("/hello")
//    @SendTo("/topic/greetings")
//    public Greeting greeting(HelloMessage message) throws Exception {
//        System.out.println("mesg" + message);
//        return new Greeting("Hello, " + message.getName() + "!");
//    }

    @MessageMapping("/greeting")
        public String handle(String greeting) {
            return "[" + getTimestamp() + ": " + greeting;
        }


    public String getTimestamp() {
        return "99999";
    }
}
